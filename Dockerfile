FROM alpine

ARG K8S_VERSION=v1.19.3
ARG HELM_VERSION=v3.4.0

RUN apk update && apk add curl

RUN cd /usr/bin && curl -LO "https://storage.googleapis.com/kubernetes-release/release/$K8S_VERSION/bin/linux/amd64/kubectl" && chmod 755 kubectl

# RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/arm64/kubectl"

RUN cd /tmp && curl -o helm.tar.gz -L "https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz" && \
    tar xzf helm.tar.gz && mv linux-amd64/helm /usr/bin/helm && rm -rf helm* linux*

# https://get.helm.sh/helm-v3.4.0-linux-amd64.tar.gz